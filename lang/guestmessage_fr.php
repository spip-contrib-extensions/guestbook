<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_guestmessage' => 'Ajouter ce message livre d\'or',

	'action_proposer_message' => 'Proposer',
	'action_publier_message' => 'Publier',
	'action_refuser_message' => 'Refuser',
	'action_supprimer_message' => 'Poubelle',

	// E
	'explication_guestmessage' => 'Texte du message de livre d\'or',
	'explication_ip' => 'Identification de l\'ordi de saisie',
	'explication_note' => 'De 0 à 10',

	//F
	'formulaire_attention_dix_caracteres' => 'Le message doit contenir au moins 10 caractères !',

	// I
	'icone_creer_guestmessage' => 'Créer un message livre d\'or',
	'icone_modifier_guestmessage' => 'Modifier ce message livre d\'or',
	'info_1_guestmessage' => 'Un message livre d\'or',
	'info_aucun_guestmessage' => 'Aucun message livre d\'or',
	'info_guestmessages_auteur' => 'Les messages livre d\'or de cet auteur',
	'info_nb_guestmessages' => '@nb@ messages livre d\'or',

	// L
	'label_date' => 'Date',
	'label_email' => 'Email',
	'label_guestmessage' => 'Message',
	'label_ip' => 'Adresse IP',
	'label_nom' => 'Nom',
	'label_note' => 'Note',
	'label_prenom' => 'Prénom',
	'label_pseudo' => 'Pseudo',
	'label_ville' => 'Ville',

	'label_reponse' => 'Réponse',

	'lien_repondre_message' => 'Répondre',

	// R
	'retirer_lien_guestmessage' => 'Retirer ce message livre d\'or',
	'retirer_tous_liens_guestmessages' => 'Retirer tous les messages livre d\'or',

	// T
	'texte_ajouter_guestmessage' => 'Ajouter un message livre d\'or',
	'texte_changer_statut_guestmessage' => 'Ce message livre d\'or est :',
	'texte_creer_associer_guestmessage' => 'Créer et associer un message livre d\'or',
	'titre_guestmessage' => 'Message livre d\'or',
	'titre_guestmessages' => 'Messages livre d\'or',
	'titre_guestmessages_rubrique' => 'Messages livre d\'or de la rubrique',
	'titre_langue_guestmessage' => 'Langue de ce message livre d\'or',
	'titre_logo_guestmessage' => 'Logo de ce message livre d\'or',

	'texte_mombre_messages_livre' => 'messages de livre d\'or',
	'texte_moyenne_publie' => 'Moyenne des messages publiés',
	'texte_moyenne_tous' => 'Moyenne des messages proposés et publiés',
	'texte_statut_propose' => 'Statut Proposé',
	'texte_statut_publie' => 'Statut Publié',
	'texte_statut_refuse' => 'Statut Refusé',
	'texte_statut_poubelle' => 'Statut Poubelle',
	
	'texte_message_poste_ok' => 'Merci d\'avoir proposé votre message. Il a bien été pris en compte.',
	'texte_erreur_traiter_post' => 'Une erreur est apparue à l\'envoi de votre message. Il est probable qu\il n\'ait pas été pris en compte. Veuillez nous en excuser.',


);

?>