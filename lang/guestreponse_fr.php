<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_guestreponse' => 'Ajouter cette réponse guestbook',

	// E
	'explication_date' => 'Date de réponse',
	'explication_guestreponse' => 'Réponse de l\'auteur au message de Livre d\'Or',
	'explication_id_auteur' => 'Id d\'auteur répondant au message',
	'explication_id_guestmessage' => 'Id de message recevant la réponse',

	// I
	'icone_creer_guestreponse' => 'Créer une réponse guestbook',
	'icone_modifier_guestreponse' => 'Modifier cette réponse guestbook',
	'info_1_guestreponse' => 'Une réponse guestbook',
	'info_aucun_guestreponse' => 'Aucune réponse guestbook',
	'info_guestreponses_auteur' => 'Les réponses livre d\'or de cet auteur',
	'info_nb_guestreponses' => '@nb@ réponses livre d\'or',

	// L
	'label_date' => 'Date',
	'label_guestreponse' => 'Réponse',
	'label_id_auteur' => 'Id auteur',
	'label_id_guestmessage' => 'Id du message auquel on repond',

	// R
	'retirer_lien_guestreponse' => 'Retirer cette réponse guestbook',
	'retirer_tous_liens_guestreponses' => 'Retirer toutes les réponses livre d\'or',

	// T
	'texte_ajouter_guestreponse' => 'Ajouter une réponse guestbook',
	'texte_changer_statut_guestreponse' => 'Cette réponse guestbook est :',
	'texte_creer_associer_guestreponse' => 'Créer et associer une réponse guestbook',
	'titre_guestreponse' => 'Réponse guestbook',
	'titre_guestreponses' => 'Réponses livre d\'or',
	'titre_guestreponses_rubrique' => 'Réponses livre d\'or de la rubrique',
	'titre_langue_guestreponse' => 'Langue de cette réponse guestbook',
	'titre_logo_guestreponse' => 'Logo de cette réponse guestbook',
);

?>