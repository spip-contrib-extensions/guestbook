<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'action_saisir_infos_perso' => 'Veuillez saisir vos coordonnées SVP',
	
	// D
	'description_liste_guestmessages' => 'Messages de Livre d\'Or du plugin Guestbook, réponses',

	// G
	'guestbook_titre' => 'Guestbook',

	// I
	'infos_perso' => 'Coordonnées',

	// L
	'label_afficher_pseudo' => 'Afficher le pseudo',
	'label_afficher_prenom' => 'Afficher le prénom',
	'label_afficher_nom' => 'Afficher le nom',
	'label_afficher_ville' => 'Afficher la ville',
	'label_afficher_date' => 'Afficher la date',
	'label_afficher_note' => 'Afficher la note',
	'label_afficher_reponses' => 'Afficher les réponses',


	// N
	'note' => 'Note',
	'note_message' => 'Evaluation et message',
	'nom_liste_guestmessages' => 'Liste de messages Guestbook',

	// P
	'publie' => 'publié',
	'publies' => 'publiés',

	// S
	'statut' => 'Statut',

	//T
	'texte_laisser_message' => 'Laisser un message...',
	'texte_aucun_message_publie' => 'Aucun message n\'est publié.',
	'titre_messages_livredor' => 'Messages du Livre d\'Or',
	'titre_formulaire_livredor' => 'Formulaire du Livre d\'Or',

);

?>